<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WeatherController;

Route::get('/', [WeatherController::class, 'home'])->name('weather.home');
Route::get('/search', [WeatherController::class, 'search'])->name('weather.search');
Route::post('/hourly', [WeatherController::class, 'hourly'])->name('weather.hourly');
Route::post('/weakly', [WeatherController::class, 'weakly'])->name('weather.weakly');

