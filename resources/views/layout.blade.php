<!-- resources/views/layout.blade.php -->

<!DOCTYPE html>
<html>
    <head>
        <title>Weather Forecast</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container mt-5">
            @yield('content')
        </div>
    </body>
</html>
