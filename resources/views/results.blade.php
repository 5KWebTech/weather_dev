<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Weather Forecast</title>
   <style>
       
       * {
  box-sizing: border-box;
  margin: 0;
  padding: 0;
}

body {
  font-family: Arial, sans-serif;
  font-size: 16px;
  line-height: 1.5;
  color: #444;
}

header {
  background-color: #007bff;
  color: #fff;
  padding: 1rem;
  text-align: center;
}

h1 {
  font-size: 2rem;
}

#location {
  text-transform: capitalize;
}

.weather-info {
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  margin: 2rem auto;
  max-width: 800px;
}

.weather-item {
  text-align: center;
  margin: 1rem;
  padding: 1rem;
  background-color: #f7f7f7;
  border-radius: 5px;
  box-shadow: 0px 2px 6px rgba(0, 0, 0, 0.1);
}

.weather-item h2 {
  font-size: 1.2rem;
  margin-bottom: 1rem;
}

.buttons {
  text-align: center;
  margin: 2rem auto;
}

button {
  background-color: #4CAF50;
  border: none;
  color: white;
  padding: 10px 20px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
}
   </style>
    
    
  </head>
  <body>
    <header>
      <h1>Weather Forecast for <span id="location">{{ $location }}</span></h1>
    </header>

    <section class="weather-info">
      <div class="weather-item">
        <h2>Current temperature</h2>
         <p>Current temperature: {{ $temperature }}°C</p>
      </div>

      <div class="weather-item">
        <h2>Description</h2>
        <p>{{ $condition }}</p>
      </div>

      <div class="weather-item">
        <h2>Wind speed</h2>
        <p>{{ $windSpeed }} km/h</p>
      </div>

      <div class="weather-item">
        <h2>Humidity</h2>
        <p>{{ $humidity }}%</p>
      </div>

      <div class="weather-item">
        <h2>Feels like</h2>
        <p>{{ $feelsLike }}%</p>
      </div>
    </section>

    <section class="buttons">
      <form action="{{route('weather.hourly')}}" method="POST">
        {{ csrf_field() }}
        <input  type="hidden" name="location" value="{{ $location }}">
        <button type="submit">Next 24 Hours</button>
      </form>

      <!--<form action="{{route('weather.weakly')}}" method="POST">-->
      <!--  {{ csrf_field() }}-->
      <!--  <input  type="hidden" name="city" value="{{ $location }}">-->
      <!--  <button type="submit">Next 7 Days</button>-->
      <!--</form>-->
    </section>
  </body>
</html>
