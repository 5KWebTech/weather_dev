<!-- resources/views/home.blade.php -->

@extends('layout')

@section('content')
    <h1>Weather Forecast</h1>
    <form action="{{ route('weather.search') }}" method="GET">
        <div class="form-group">
            <label for="location">Location:</label>
            <input type="text" name="location" id="location" class="form-control" placeholder="Enter a location">
        </div>
        <button type="submit" class="btn btn-primary">Search</button>
    </form>
@endsection
