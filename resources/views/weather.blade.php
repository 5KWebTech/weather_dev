<h1>Current weather in {{ $current->name }}:</h1>
<p>{{ $current->weather[0]->description }}</p>
<p>Temperature: {{ $current->main->temp }} &deg;C</p>
<p>Humidity: {{ $current->main->humidity }}%</p>

<h1>Next 24 hours:</h1>
<ul>
    @foreach ($next24h->list as $weather)
        <li>{{ $weather->dt_txt }}: {{ $weather->weather[0]->description }}, {{ $weather->main->temp }} &deg;C</li>
    @endforeach
</ul>

<h1>Next 7 days:</h1>
<ul>
    @foreach ($next7d->list as $weather)
        <li>{{ date('l, F j', $weather->dt) }}: {{ $weather->weather[0]->description }}, {{ $weather->temp->day }} &deg;C</li>
    @endforeach
</ul>
