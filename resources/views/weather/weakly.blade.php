<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3>Weather Report for Next 7 Days</h3>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Weather</th>
                        <th>Temperature</th>
                        <th>Humidity</th>
                        <th>Wind Speed</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($weatherData['daily'] as $day)
                    <tr>
                        <td>{{ date('M d', $day['dt']) }}</td>
                        <td>{{ $day['weather'][0]['description'] }}</td>
                        <td>{{ $day['temp']['day'] }}°C</td>
                        <td>{{ $day['humidity'] }}%</td>
                        <td>{{ $day['wind_speed'] }} m/s</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
