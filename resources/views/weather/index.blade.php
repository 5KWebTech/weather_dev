@extends('layouts.app')

@section('content')
    <h1>Weather forecast</h1>

    <form method="get" action="{{ route('weather.index') }}">
        <div class="form-group">
            <label for="location">Enter a location:</label>
            <input type="text" class="form-control" name="location" id="location" value="{{ $location ?? '' }}">
        </div>
        <button type="submit" class="btn btn-primary">Search</button>
    </form>

    @if ($location)
        <h2>Weather forecast for {{ $location }}</h2>

        <h3>Next 24 hours:</h3>
        @foreach ($next24Hours as $forecast)
            <p>{{ date('H:i', $forecast['dt']) }}: {{ $forecast['main']['temp'] }}°C, {{ $forecast['weather'][0]['description'] }}</p>
        @endforeach

        <h3>Next 7 days:</h3>
        @foreach ($dailyForecast['list'] as $forecast)
            <p>{{ date('l', $forecast['dt']) }}: {{ $forecast['temp']['min'] }}°C - {{ $forecast['temp']['max'] }}°C, {{ $forecast['weather'][0]['description'] }}</p>
        @endforeach
    @endif
@endsection
