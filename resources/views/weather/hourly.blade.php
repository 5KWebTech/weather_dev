<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hourly Weather Forecast</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            font-size: 16px;
        }
        h1 {
            text-align: center;
        }
        table {
            border-collapse: collapse;
            margin: 0 auto;
        }
        table, th, td {
            border: 1px solid black;
        }
        th {
            background-color: #ddd;
        }
        th, td {
            padding: 10px;
            text-align: center;
        }
    </style>
</head>
<body>
    <h1>Hourly Weather Forecast for {{ $location }}</h1>
    <table>
        <thead>
            <tr>
                <th>Date/Time</th>
                <th>Temperature (��C)</th>
                <th>Description</th>
                <th>Wind Speed (m/s)</th>
                <th>Humidity (%)</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($hourlyData as $hourly)
                <tr>
                    <td>{{ $hourly['date'] }}</td>
                    <td>{{ $hourly['temperature'] }}</td>
                    <td>{{ $hourly['condition'] }}</td>
                    <td>{{ $hourly['windSpeed'] }}</td>
                    <td>{{ $hourly['humidity'] }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>
