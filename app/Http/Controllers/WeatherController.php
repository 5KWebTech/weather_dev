<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Http;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class WeatherController extends Controller
{
    public function home()
    {
        return view('home');
    }

    public function search(Request $request)
    {
        $location = $request->input('location');

        $client = new Client();
        $apiKey = config('services.openweather.api_key');

      
        $url = "https://api.openweathermap.org/data/2.5/weather?q=$location&units=metric&appid=$apiKey";

        try {
            $response = $client->request('GET', $url);
            $data = json_decode($response->getBody(), true);

            $temperature = $data['main']['temp'];
            $condition = $data['weather'][0]['description'];
            $windSpeed = $data['wind']['speed'];
            $humidity = $data['main']['humidity'];
            $feelsLike = $data['main']['feels_like'];

            return view('results', compact('location', 'temperature', 'condition', 'windSpeed', 'humidity','feelsLike'));
        } catch (\Exception $e) {
            $errorMessage = "Error fetching weather data: " . $e->getMessage();
            return view('error', compact('errorMessage'));
        }
    }

    public function hourly(Request $request)
    {

       
        $location = $request->location;

       
        $apiKey = config('services.openweather.api_key');

         $now = time();



         $end = $now + 86400;
      
    
        // Construct the API URL for the hourly forecast for the next 24 hours
        // $url = "https://api.openweathermap.org/data/2.5/forecast/?q=$location&units=metric&cnt=8&appid=$apiKey";


        $url = "https://api.openweathermap.org/data/2.5/forecast/?q=$location&units=metric&appid=$apiKey&cnt=8&start=$now&end=$end";

    
    
        // Use GuzzleHttp to make the API request
        $client = new \GuzzleHttp\Client();
        $response = $client->get($url);
        $data = json_decode($response->getBody(), true);
    
        // Extract the hourly forecast data from the API response
        $hourlyData = [];
        foreach ($data['list'] as $hourly) {
            $hourlyData[] = [
                'date' => date('d-m-Y / h a', $hourly['dt']),                'temperature' => $hourly['main']['temp'],
                'condition' => $hourly['weather'][0]['description'],
                'windSpeed' => $hourly['wind']['speed'],
                'humidity' => $hourly['main']['humidity'],
            ];
        }
    
        // Pass the hourly forecast data to the view
        return view('weather.hourly', compact('location', 'hourlyData'));
    }
    

  


    public function weakly(Request $request)
{
    $apiKey = config('services.openweather.api_key');

    $city = 'London';
    $countryCode = 'uk';

    // Get current weather data for the city
    $url = "http://api.openweathermap.org/data/2.5/weather?q=$city,$countryCode&appid=$apiKey&units=metric";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $weatherData = json_decode(curl_exec($ch), true);

    if ($weatherData['cod'] == 200) {
        $lat = $weatherData['coord']['lat'];
        $lon = $weatherData['coord']['lon'];
        $exclude = 'current,minutely,hourly';
        // Get 7-day forecast data for the city
        $url = "http://api.openweathermap.org/data/2.5/onecall?lat=51.5085&lon=-0.1257&exclude=current,minutely,hourly&appid=fdaad09966b258af1d93af3b449760ff&units=metric";
        dd($url);




        dd($url);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $weatherData = json_decode(curl_exec($ch), true);

        return view('weather.weakly', ['weatherData' => $weatherData]);
    } else {
        return view('weather.weakly', ['weatherData' => null]);
    }
}
}



